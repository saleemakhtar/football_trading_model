#' Title
#'
#' @param epl_data_elos 
#' @param row_data 
#'
#' @return
#' @export
#'
#' @examples
get_elo_metrics <- function(dataset, row_data, h_or_a) {
  if (h_or_a == "Away"){
    test_home_goals <- dataset$FTHG
    dataset$FTHG <- dataset$FTAG
    dataset$FTAG <- test_home_goals
    dataset$elo_away <- dataset$elo_home
  }
  df <- filter(
    dataset,
    dataset[, paste0(h_or_a, "Team")] == row_data[, paste0(h_or_a, "Team")], 
    dataset$Date < row_data$Date
  ) %>% 
    arrange(desc(Date)) %>%
    head(3) %>%
    summarise(
      elo_goals_scored = sum(FTHG * elo_away),
      elo_goals_conceded = sum(FTAG * elo_away),
      elo_shots = sum(HS * elo_away),
      elo_shots_conceded = sum(AS * elo_away),
      elo_SOT = sum(HST * elo_away),
      elo_SOT_conceded = sum(AST * elo_away),
      elo_corners = sum(HC * elo_away),
      elo_corners_conceded = sum(AC * elo_away)
    )
    
  names(df) <- c(
    paste0(h_or_a, "_elo_goals_scored"), 
    paste0(h_or_a, "_elo_goals_conceded"),
    paste0(h_or_a, "_elo_shots"),
    paste0(h_or_a, "_elo_shots_conceded"),
    paste0(h_or_a, "_elo_SOT"),
    paste0(h_or_a, "_elo_SOT_conceded"),
    paste0(h_or_a, "_elo_corners"),
    paste0(h_or_a, "_elo_corners_conceded")
  )
  df
}
